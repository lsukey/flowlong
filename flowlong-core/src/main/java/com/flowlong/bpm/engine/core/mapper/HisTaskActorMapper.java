package com.flowlong.bpm.engine.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flowlong.bpm.engine.entity.HisTaskActor;

public interface HisTaskActorMapper extends BaseMapper<HisTaskActor> {

}
